package pe.net.tci.efacturacion.ose.gcpstorageservice.repository;

import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import pe.net.tci.efacturacion.ose.commons.db.comprobante.tables.TxTicketArchivo;
import pe.net.tci.efacturacion.ose.commons.db.comprobante.tables.records.TxTicketArchivoRecord;

@RequiredArgsConstructor
@Repository
public class TxTicketArchivoRepository {

  private final DSLContext dslContext;
  private final TxTicketArchivo TX_TICKET_ARCHIVO = TxTicketArchivo.TX_TICKET_ARCHIVO;

  public TxTicketArchivoRecord nuevoRecord() {
    return dslContext.newRecord(TX_TICKET_ARCHIVO);
  }

  public void registarTicketXml(TxTicketArchivoRecord txTicketArchivoRecord) {
    dslContext.executeInsert(txTicketArchivoRecord);
  }

  public TxTicketArchivoRecord obtenerPorTicket(String ticket) {
    return dslContext.fetchOne(TX_TICKET_ARCHIVO, TX_TICKET_ARCHIVO.TICKET.eq(ticket));
  }

  public void eliminarRegistroPorTicket(String ticket) {
    dslContext
      .deleteFrom(TX_TICKET_ARCHIVO)
      .where(TX_TICKET_ARCHIVO.TICKET.eq(ticket))
      .execute();
  }
}
