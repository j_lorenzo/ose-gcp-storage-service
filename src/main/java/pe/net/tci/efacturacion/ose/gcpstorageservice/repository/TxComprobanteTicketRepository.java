package pe.net.tci.efacturacion.ose.gcpstorageservice.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import pe.net.tci.efacturacion.ose.commons.base.domain.EstadoSolicitud;
import pe.net.tci.efacturacion.ose.commons.base.domain.EstadoTicket;
import pe.net.tci.efacturacion.ose.commons.base.exception.SunatException;
import pe.net.tci.efacturacion.ose.commons.db.comprobante.tables.TxComprobanteTicket;
import pe.net.tci.efacturacion.ose.commons.db.comprobante.tables.TxSolicitud;
import pe.net.tci.efacturacion.ose.commons.db.comprobante.tables.TxSolicitudError;
import pe.net.tci.efacturacion.ose.commons.db.comprobante.tables.records.TxSolicitudErrorRecord;

import java.time.LocalDateTime;

@RequiredArgsConstructor
@Repository
public class TxComprobanteTicketRepository {

  private final ObjectMapper objectMapper;
  private final DSLContext dslContext;

  public void invalidarTicket(String idTraza, String ticket, SunatException se) {
    TxSolicitud sol = TxSolicitud.TX_SOLICITUD;
    TxComprobanteTicket tic = TxComprobanteTicket.TX_COMPROBANTE_TICKET;

    LocalDateTime now = LocalDateTime.now();

    int wasTicketUpdated = dslContext.update(tic)
        .set(tic.ESTADO_TICKET, EstadoTicket.INVALIDO.code())
        .set(tic.FECHA_ACTUALIZACION, now)
        .where(tic.NUMERO_TICKET.eq(ticket))
        .execute();

    dslContext.update(sol)
        .set(sol.EST_DOCUMENTO, EstadoSolicitud.INVALIDO.code())
        .set(sol.FEC_MODIFICA, now)
        .where(sol.ID_TRACE.eq(idTraza))
        .execute();

    if (wasTicketUpdated > 0) {
      TxSolicitudErrorRecord errorRecord = dslContext.newRecord(TxSolicitudError.TX_SOLICITUD_ERROR);
      errorRecord.setIdTrace(idTraza);
      errorRecord.setCodigoError(se.getErrorSunat().getCodigo());

      try {
        errorRecord.setMensajeError(objectMapper.writeValueAsBytes(se.getErrorSunat()));

      } catch (JsonProcessingException jsonpe) {
        String message = "Mensaje de error no disponible. Causa: " + jsonpe.getMessage();
        errorRecord.setMensajeError(message.getBytes());
      }

      errorRecord.setFecRegistro(LocalDateTime.now());
      dslContext.executeInsert(errorRecord);
    }
  }
}
