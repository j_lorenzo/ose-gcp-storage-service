package pe.net.tci.efacturacion.ose.gcpstorageservice.processor;

import brave.Tracer;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.StorageException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import pe.net.tci.efacturacion.ose.commons.base.constants.OseConstants;
import pe.net.tci.efacturacion.ose.commons.base.dto.ErrorSunat;
import pe.net.tci.efacturacion.ose.commons.base.exception.SunatException;
import pe.net.tci.efacturacion.ose.commons.db.comprobante.tables.records.TxComprobanteArchivoRecord;
import pe.net.tci.efacturacion.ose.commons.db.comprobante.tables.records.TxTicketArchivoRecord;
import pe.net.tci.efacturacion.ose.gcpstorageservice.dto.ArchivoStorage;
import pe.net.tci.efacturacion.ose.gcpstorageservice.excepcion.DefaultErrorException;
import pe.net.tci.efacturacion.ose.gcpstorageservice.repository.TxTicketArchivoRepository;
import pe.net.tci.efacturacion.ose.gcpstorageservice.service.ComprobanteArchivoService;
import pe.net.tci.efacturacion.ose.gcpstorageservice.service.ResumenService;
import pe.net.tci.efacturacion.ose.gcpstorageservice.service.StorageService;
import pe.net.tci.efacturacion.ose.gcpstorageservice.stream.ComprobanteErrorChannel;
import pe.net.tci.efacturacion.ose.gcpstorageservice.stream.SendTicket;
import pe.net.tci.efacturacion.ose.gcpstorageservice.stream.StorageServiceChannel;
import pe.net.tci.efacturacion.ose.gcpstorageservice.util.UUIDUtil;
import pe.net.tci.efacturacion.ose.gcpstorageservice.util.Utilitario;

import javax.validation.Valid;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
@AllArgsConstructor
public class StorageConsumer {

  private final ComprobanteArchivoService comprobanteArchivoService;
  private final StorageService storageService;
  private final TxTicketArchivoRepository ticketArchivoRepository;
  private final ResumenService resumenService;
  private final StreamBridge streamBridge;
  private final Tracer tracer;

  @StreamListener(StorageServiceChannel.INPUT)
  public void procesarArchivosStorage(@Payload @Valid String idNomArchivoXml,
                                      @Header(name = "x-death", required = false) Map<?, ?> deathHeader) {

    log.info("NomArchivo XML recibido: {}", idNomArchivoXml);
    TxComprobanteArchivoRecord txComprobanteArchivoRecord;
    String ublVersion = "";
    String nomArchivoXmlOZip = "";
    ArchivoStorage archivoStorage;
    try {
      txComprobanteArchivoRecord = comprobanteArchivoService.obtenerPorId(idNomArchivoXml)
          .orElseThrow(() -> new DefaultErrorException("No existe un comprobante archivo para el NomArchivo: " + idNomArchivoXml));

      ublVersion = txComprobanteArchivoRecord.getUblVersion();

      if (Byte.valueOf("0").compareTo(txComprobanteArchivoRecord.getWsStorageXml()) == 0) {
        nomArchivoXmlOZip = txComprobanteArchivoRecord.getNomArchivoXml();
        byte[] byteArchivo = Utilitario.procesarArchivoZip(txComprobanteArchivoRecord.getArchivoZip(),txComprobanteArchivoRecord.getNomArchivoXml() );
        archivoStorage = new ArchivoStorage(nomArchivoXmlOZip, ublVersion, byteArchivo, "xml/ubl", "xml");
        log.info("Actualizando en BD estado archivo {} XML a guardado en google storage.", nomArchivoXmlOZip);
        storageService.guardarArchivoStorage(archivoStorage);
        comprobanteArchivoService.actualizarEstadoArchivoAGuardadoEnStorage(idNomArchivoXml, "ARCHIVO_XML");
      }

      if (Byte.valueOf("0").compareTo(txComprobanteArchivoRecord.getWsStorageCdr()) == 0) {
        nomArchivoXmlOZip = txComprobanteArchivoRecord.getNomCdrZip();
        byte[] byteArchivo = Utilitario.procesarArchivoZip(txComprobanteArchivoRecord.getArchivoZip(),txComprobanteArchivoRecord.getNomArchivoXml() );
        archivoStorage = new ArchivoStorage(nomArchivoXmlOZip, ublVersion, byteArchivo, "cdr", "cdr");
        log.info("Actualizando en BD estado archivo {} XML a guardado en google storage.", nomArchivoXmlOZip);
        storageService.guardarArchivoStorage(archivoStorage);
        log.info("Actualizando en BD estado archivo {} CDR a guardado en storage.", nomArchivoXmlOZip);
        comprobanteArchivoService.actualizarEstadoArchivoAGuardadoEnStorage(idNomArchivoXml, "ARCHIVO_CDR");
      }

    } catch (Exception e) {
      archivoStorage = new ArchivoStorage(nomArchivoXmlOZip, ublVersion, null, "xml/ubl", "xml");
      this.evaluarStorageException(archivoStorage, nomArchivoXmlOZip, deathHeader, e);
    }
  }

  @StreamListener(StorageServiceChannel.INPUT_TICKET)
  public void procesarTicketStorage(@Payload @Valid SendTicket sendTicketInput,
                                    @Header(name = "x-death", required = false) Map<?, ?> deathHeader) {
    log.info("Recibiendo ticket [{}] para registrar xml [{}] en storage", sendTicketInput.getTicket(),sendTicketInput.getResumenId().getNombre());
    try{
      /** Guardar xml del ticket en storage*/
      TxTicketArchivoRecord ticketArchivoRecord = ticketArchivoRepository.obtenerPorTicket(sendTicketInput.getTicket());
      ArchivoStorage archivoStorage = new ArchivoStorage(sendTicketInput.getResumenId().getNombre(), null, ticketArchivoRecord.getArchivoXml(), "ticket", "ticket");
      storageService.guardarArchivoStorage(archivoStorage);
      /**Enviar mensaje para procesarTicket*/
      streamBridge.send(ComprobanteErrorChannel.OUTPUT, MessageBuilder.withPayload(sendTicketInput).build());
      /**Eliminar registro de la bd*/
      ticketArchivoRepository.eliminarRegistroPorTicket(sendTicketInput.getTicket());
    } catch (Exception e) {
      Long contadorReintentos = (deathHeader != null) ? ((Long) deathHeader.get("count")) : 0L;
      if (contadorReintentos.compareTo(5L) > 0) {
        log.error("Maximo numero de reintentos alcanzado. Revisar XML del ticket [{}]: . Excepcion: {}.", sendTicketInput.getTicket(), e.getMessage(), e);
        String idTraza = UUIDUtil.crearIdTraza(tracer);
        resumenService.invalidarTicket(idTraza, sendTicketInput.getTicket(), this.getDefaultErrorSunat(idTraza));
      } else {
        log.warn("[Reintento {}]. En el proceso de guardado del NomArchivo: {} en google storage. Excepcion: {}.", contadorReintentos, sendTicketInput.getResumenId().getNombre(), e.getMessage());
        throw new AmqpRejectAndDontRequeueException(e.getMessage());
      }
    }
  }

  private void evaluarStorageException(ArchivoStorage archivoStorage, String nomArchivoXmlOZip, Map<?, ?> deathHeader, Exception e) {
    Long contadorReintentos = (deathHeader != null) ? ((Long) deathHeader.get("count")) : 0L;

    if (e instanceof DefaultErrorException) {
      log.error("No existe el IdNomArchivoXml: {} en la tabla TxComprobanteArchivo.", archivoStorage.getNombreArchivo());
    } else {
      evaluarStorageException(e, archivoStorage, nomArchivoXmlOZip);
      if (contadorReintentos.compareTo(10L) > 0) {
        log.error("Maximo numero de reintentos alcanzado. Revisar el estado de guardado de los archivos con NomArchivoXml: {}. Excepcion: {}.", archivoStorage.getNombreArchivo(), e.getMessage(), e);
      } else {
        log.warn("[Reintento {}]. En el proceso de guardado del NomArchivo: {} en google storage. Excepcion: {}.", contadorReintentos, nomArchivoXmlOZip, e.getMessage());
        throw new AmqpRejectAndDontRequeueException(e.getMessage());
      }
    }
  }

  private void evaluarStorageException(Exception e, ArchivoStorage archivoStorage, String nomArchivoXmlOZip) {
    if (e instanceof StorageException && e.getMessage().contains("Precondition Failed")) {
      Optional<Blob> opBlobArchivo = nomArchivoXmlOZip.endsWith("xml") ? storageService.obtenerArchivoStorage(archivoStorage, "xml") : storageService.obtenerArchivoStorage(archivoStorage, "cdr");
      if (opBlobArchivo.isPresent()) {
        if (nomArchivoXmlOZip.endsWith("xml")) {
          log.info("[StorageExcepcion-Precondition Failed]. Existe el archivo {} en el bucket, actualizando en BD estado archivo XML a guardado en google storage.", nomArchivoXmlOZip);
          comprobanteArchivoService.actualizarEstadoArchivoAGuardadoEnStorage(archivoStorage.getNombreArchivo(), "ARCHIVO_XML");
        } else {
          log.info("[StorageExcepcion-Precondition Failed]. Existe el archivo {} en el bucket, actualizando en BD estado archivo CDR a guardado en google storage.", nomArchivoXmlOZip);
          comprobanteArchivoService.actualizarEstadoArchivoAGuardadoEnStorage(archivoStorage.getNombreArchivo(), "ARCHIVO_CDR");
        }
      }
    }
  }

  private SunatException getDefaultErrorSunat(String idTrace) {
    ErrorSunat es = OseConstants.DEFAULT_ERROR;
    ErrorSunat.DescripcionAdicional da = new ErrorSunat.DescripcionAdicional();
    da.setDescripcion("Revisar logs del sistema mediante idTraza " + idTrace);
    es.setDescripcionAdicional(da);

    return new SunatException(es);
  }
}
