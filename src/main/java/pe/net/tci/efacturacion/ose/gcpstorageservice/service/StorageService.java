package pe.net.tci.efacturacion.ose.gcpstorageservice.service;

import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import pe.net.tci.efacturacion.ose.gcpstorageservice.dto.ArchivoStorage;
import pe.net.tci.efacturacion.ose.gcpstorageservice.properties.AppProperties;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class StorageService {

  private final Storage storage;
  private final AppProperties appProperties;

  public void guardarArchivoStorage(ArchivoStorage archivoStorage){

    String path = "";
    if(archivoStorage.getTipo().equals("xml")){
      path = String.join("/", archivoStorage.getPath().concat(archivoStorage.getUblVersion()),
          archivoStorage.getNombreArchivo());
      log.info("Persistiendo XML {} en bucket {} -> path {}", archivoStorage.getNombreArchivo(), appProperties.getCloudStorage().getRepositorio(archivoStorage.getTipo()), path);
    } else if(archivoStorage.getTipo().equals("cdr")) {
      path = String.join("/", "cdr", archivoStorage.getNombreArchivo());
      log.info("Persistiendo CDR {} en bucket {} -> path {}", archivoStorage.getNombreArchivo(), appProperties.getCloudStorage().getRepositorio(archivoStorage.getTipo()), path);
    } else if(archivoStorage.getTipo().equals("ticket")){
      path = String.join("/", archivoStorage.getPath(), archivoStorage.getNombreArchivo());
      log.info("Persistiendo temp (ticket) {} en bucket {} -> path {}", archivoStorage.getPath(), appProperties.getCloudStorage().getRepositorio(archivoStorage.getTipo()), path);
    }

    byte[] file = archivoStorage.getArchivoBase64()==null ? archivoStorage.getArchivo() :Base64.getEncoder().encode(archivoStorage.getArchivoBase64().getBytes());

    BlobInfo archivoBlobInfo = BlobInfo.newBuilder(BlobId.of(appProperties.getCloudStorage().getRepositorio(archivoStorage.getTipo()), path))
        .setContentType(archivoStorage.getTipo().equals("xml") ? MediaType.TEXT_XML_VALUE : "application/zip")
        .setContentEncoding(StandardCharsets.UTF_8.name())
        .setMetadata(almacenarMetadata(archivoStorage))
        .build();

    if (appProperties.getCloudStorage().isPermitirXmlDuplicado()) {
      storage.create(archivoBlobInfo, file);
    } else {
      storage.create(archivoBlobInfo, file, Storage.BlobTargetOption.doesNotExist());
    }

  }

  public Optional<Blob> obtenerArchivoStorage(ArchivoStorage archivoStorage, String tipo){

    String path;
    String repositorio;
    if(tipo.equals("xml")){
      repositorio = appProperties.getCloudStorage().getRepositorioXml();
      path = String.join("/", archivoStorage.getPath().concat(archivoStorage.getUblVersion()),
          archivoStorage.getNombreArchivo());
    }else {
      path = String.join("/", "cdr", archivoStorage.getNombreArchivo());
      repositorio = appProperties.getCloudStorage().getRepositorioXml();
    }

    return Optional.ofNullable(storage.get(BlobId.of(repositorio, path)));
  }

  public void eliminarArchivoStorage(){

  }

  public Map<String, String> almacenarMetadata (ArchivoStorage archivoStorage) {

    Map<String, String> metadata = new HashMap<>();
    if(archivoStorage.getTipo().equals("xml")){
      metadata.put("ubl", archivoStorage.getUblVersion());
      metadata.put("type", archivoStorage.getTipo());
    }

    if(archivoStorage.getTipo().equals("ticket")){
      metadata.put("nombreOriginal", archivoStorage.getNombreArchivo() + ".xml");
    }

    if(archivoStorage.getTipo().equals("cdr")){
      metadata.put("type", "cdr");
    }
    return metadata;

  }

}
