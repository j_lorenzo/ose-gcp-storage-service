package pe.net.tci.efacturacion.ose.gcpstorageservice.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

  @Value("${actuator-security.username}")
  private String username;

  @Value("${actuator-security.password}")
  private String password;

  @Autowired
  private Environment environment;

  @Override
  @SuppressWarnings("Duplicates")
  public void configure(HttpSecurity httpSecurity) throws Exception {
    BasicAuthenticationEntryPoint authenticationEntryPoint = new BasicAuthenticationEntryPoint();
    authenticationEntryPoint.setRealmName("ose-realm");

    httpSecurity.csrf().disable();
    httpSecurity.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    httpSecurity.httpBasic().authenticationEntryPoint(authenticationEntryPoint);

    if (environment.containsProperty("management.endpoints.web.exposure.include")) {
      String[] availableEndpoints = environment.getRequiredProperty("management.endpoints.web.exposure.include")
          .split(",");

      httpSecurity.authorizeRequests()
          .requestMatchers(EndpointRequest.to(availableEndpoints)).permitAll();
    }

    httpSecurity.headers().cacheControl().disable();
  }

  @Bean
  protected UserDetailsService userDetailsService() {
    PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
    InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
    manager.createUser(User.withUsername(username)
        .password(encoder.encode(password))
        .roles("ACTUATOR")
        .build());
    return manager;
  }
}
