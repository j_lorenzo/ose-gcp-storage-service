package pe.net.tci.efacturacion.ose.gcpstorageservice.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Utilitario {

  public static final String PATH_XML_STORAGE = "xml/ubl";
  public static final String PATH_TICKET_STORAGE = "ticket";
  public static final String PATH_CDR_STORAGE = "cdr";

  public static byte[] procesarArchivoZip(byte[] archivoZip, String nombreArchivo) throws IOException {

    ZipInputStream zipStream = new ZipInputStream(new ByteArrayInputStream(archivoZip));
    byte[] responseFile = new byte[0];
    try{
      ZipEntry entry = null;
      while ((entry = zipStream.getNextEntry()) != null) {

        String fileName = entry.getName();
        String nombreCdr = "R-"+nombreArchivo.replace("zip", "xml");

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int count;

        while ((count = zipStream.read(buffer)) != -1) {
          baos.write(buffer, 0, count);
        }

        baos.close();
        zipStream.closeEntry();
        if(fileName.equals(nombreArchivo) || fileName.equals(nombreCdr)){
          responseFile = baos.toByteArray();
        }
      }
    } finally{
      zipStream.close();
    }

    return responseFile;
  }
}
