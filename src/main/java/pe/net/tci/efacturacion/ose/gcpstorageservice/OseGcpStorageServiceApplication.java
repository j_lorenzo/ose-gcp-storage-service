package pe.net.tci.efacturacion.ose.gcpstorageservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.stream.annotation.EnableBinding;
import pe.net.tci.efacturacion.ose.gcpstorageservice.stream.StorageServiceChannel;

@SpringBootApplication
@EnableDiscoveryClient
@EnableBinding(value = {StorageServiceChannel.class})
public class OseGcpStorageServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OseGcpStorageServiceApplication.class, args);
	}

}
