package pe.net.tci.efacturacion.ose.gcpstorageservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pe.net.tci.efacturacion.ose.commons.db.comprobante.tables.records.TxComprobanteArchivoRecord;
import pe.net.tci.efacturacion.ose.gcpstorageservice.repository.TxComprobanteArchivoRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ComprobanteArchivoService {

  private final TxComprobanteArchivoRepository comprobanteArchivoRepository;

  public void registrarArchivosXmlYCdr(String nomArchivoXml, String nomCdrZip, String ublVersion, byte[] archivoXml, byte[] archivoZip) {
    TxComprobanteArchivoRecord txComprobanteArchivoRecord = comprobanteArchivoRepository.nuevoRecord();
    txComprobanteArchivoRecord.setNomArchivoXml(nomArchivoXml);
    txComprobanteArchivoRecord.setNomCdrZip(nomCdrZip);
    txComprobanteArchivoRecord.setUblVersion(ublVersion);
    txComprobanteArchivoRecord.setArchivoXml(archivoXml);
    txComprobanteArchivoRecord.setArchivoZip(archivoZip);
    txComprobanteArchivoRecord.setFecRegistro(LocalDateTime.now());
    comprobanteArchivoRepository.registarArchivosXmlYCdr(txComprobanteArchivoRecord);
  }

  public Optional<TxComprobanteArchivoRecord> obtenerPorId(String nomArchivoXml) {
    return Optional.ofNullable(comprobanteArchivoRepository.obtenerPorNomArchivoXml(nomArchivoXml));
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void actualizarEstadoArchivoAGuardadoEnStorage(String nomArchivoXml, String tipoArchivo) {
    comprobanteArchivoRepository.actualizarEstadoArchivoAGuardadoEnStorage(nomArchivoXml, tipoArchivo);
  }

  @Transactional
  public void eliminarRegistrosGuardadosEnStorageCorrectamentePorNroDias(Integer nroDias) {
    comprobanteArchivoRepository.eliminarRegistrosGuardadosEnStorageCorrectamentePorNroDias(nroDias);
  }

  public List<String> obtenerArchivosNoGuardadosEnStorage(){
    return comprobanteArchivoRepository.obtenerArchivosNoGuardadosEnStorage();
  }
}
