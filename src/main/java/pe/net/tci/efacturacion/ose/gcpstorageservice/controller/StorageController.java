package pe.net.tci.efacturacion.ose.gcpstorageservice.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pe.net.tci.efacturacion.ose.gcpstorageservice.dto.ArchivoStorage;
import pe.net.tci.efacturacion.ose.gcpstorageservice.service.StorageService;

@RequiredArgsConstructor
@Slf4j
@RestController
public class StorageController {

  private final StorageService storageService;

  @PostMapping("/guardarArchivo")
  public void guardarArchivoStorage(@RequestBody ArchivoStorage archivoStorage){

    storageService.guardarArchivoStorage(archivoStorage);
  }

}
