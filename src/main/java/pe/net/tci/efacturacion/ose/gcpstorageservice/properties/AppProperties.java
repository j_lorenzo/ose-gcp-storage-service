package pe.net.tci.efacturacion.ose.gcpstorageservice.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;

@Component
@ConfigurationProperties(prefix = "opv")
public class AppProperties {

  private CloudStorage cloudStorage = new CloudStorage();

  public CloudStorage getCloudStorage() {
    return cloudStorage;
  }

  public void setCloudStorage(CloudStorage cloudStorage) {
    this.cloudStorage = cloudStorage;
  }

  public static class CloudStorage {

    @NotBlank
    private String repositorioXml;

    @NotBlank
    private String repositorioCdr;

    private String repositorio;

    @NotBlank
    private String repositorioTemp;

    private boolean permitirXmlDuplicado = true;

    private boolean permitirCdrDuplicado = true;

    public String getRepositorioXml() {
      return repositorioXml;
    }

    public void setRepositorioXml(String repositorioXml) {
      this.repositorioXml = repositorioXml;
    }

    public String getRepositorioCdr() {
      return repositorioCdr;
    }

    public void setRepositorioCdr(String repositorioCdr) {
      this.repositorioCdr = repositorioCdr;
    }

    public String getRepositorioTemp() {
      return repositorioTemp;
    }

    public void setRepositorioTemp(String repositorioTemp) {
      this.repositorioTemp = repositorioTemp;
    }

    public boolean isPermitirXmlDuplicado() {
      return permitirXmlDuplicado;
    }

    public void setPermitirXmlDuplicado(boolean permitirXmlDuplicado) {
      this.permitirXmlDuplicado = permitirXmlDuplicado;
    }

    public boolean isPermitirCdrDuplicado() {
      return permitirCdrDuplicado;
    }

    public void setPermitirCdrDuplicado(boolean permitirCdrDuplicado) {
      this.permitirCdrDuplicado = permitirCdrDuplicado;
    }

    public String getRepositorio(String tipo) {
      switch (tipo) {

        case "xml" :
          repositorio = this.getRepositorioXml();
          break;

        case "ticket" :
          repositorio = this.getRepositorioTemp();
          break;

        case "cdr" :
          repositorio = this.getRepositorioCdr();
          break;

        default:
          repositorio = "";
          break;
      }
      return repositorio;
    }

    public void setRepositorio(String repositorio) {
      this.repositorio = repositorio;
    }

  }
}
