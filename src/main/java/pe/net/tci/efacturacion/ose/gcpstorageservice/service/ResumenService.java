package pe.net.tci.efacturacion.ose.gcpstorageservice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pe.net.tci.efacturacion.ose.commons.base.exception.SunatException;
import pe.net.tci.efacturacion.ose.gcpstorageservice.repository.TxComprobanteTicketRepository;

@RequiredArgsConstructor
@Slf4j
@Service
public class ResumenService {

  private final TxComprobanteTicketRepository ticketRepository;

  @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
  public void invalidarTicket(String idTraza, String ticket, SunatException se) {
    ticketRepository.invalidarTicket(idTraza, ticket, se);
  }
}
