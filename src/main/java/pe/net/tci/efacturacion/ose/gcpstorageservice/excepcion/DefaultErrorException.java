package pe.net.tci.efacturacion.ose.gcpstorageservice.excepcion;

import pe.net.tci.efacturacion.ose.commons.base.rest.DefaultError;

public class DefaultErrorException extends Exception {

  private DefaultError defaultError;

  public DefaultErrorException(Throwable cause, DefaultError defaultError) {
    super(defaultError.toString(), cause);
    this.defaultError = defaultError;
  }

  public DefaultErrorException(String msj){
    super(msj);
  }

  public DefaultError getDefaultError() {
    return defaultError;
  }

  public void setDefaultError(DefaultError defaultError) {
    this.defaultError = defaultError;
  }
}
