package pe.net.tci.efacturacion.ose.gcpstorageservice.stream;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Data;
import lombok.NonNull;
import pe.net.tci.efacturacion.ose.commons.auth.rest.Usuario;
import pe.net.tci.efacturacion.ose.commons.base.domain.ResumenId;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class SendTicket {

  @NonNull
  @JsonProperty
  @NotNull(message = "El parametro 'sendBill' es invalido. El valor ingresado es nulo")
  private String ticket;

  @NonNull
  @Valid
  @JsonProperty
  @NotNull(message = "El parametro 'usuario' es invalido. El valor ingresado es nulo")
  private Usuario usuario;

  @NonNull
  @Valid
  @JsonProperty
  @NotNull(message = "El parametro 'resumenId' es invalido. El valor ingresado es nulo")
  private ResumenId resumenId;

  @NonNull
  @Valid
  @JsonProperty(required = true)
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
  @JsonDeserialize(using = LocalDateTimeDeserializer.class)
  @JsonSerialize(using = LocalDateTimeSerializer.class)
  private LocalDateTime fechaTransmision;


}
