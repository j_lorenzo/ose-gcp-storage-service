package pe.net.tci.efacturacion.ose.gcpstorageservice.stream;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface StorageServiceChannel {

  String INPUT = "archivosStorageInput";
  String OUTPUT = "archivosStorageOutput";

  String INPUT_TICKET = "ticketStorageInput";

  @Input(StorageServiceChannel.INPUT)
  SubscribableChannel archivosStorageInput();

  @Input(StorageServiceChannel.INPUT_TICKET)
  SubscribableChannel ticketStorageInput();

}
