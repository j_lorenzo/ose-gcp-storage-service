package pe.net.tci.efacturacion.ose.gcpstorageservice.util;

import brave.Tracer;
import lombok.experimental.UtilityClass;

import java.util.UUID;

@UtilityClass
public class UUIDUtil {

  public String crearIdTraza(Tracer tracer) {
    return new UUID(
        tracer.currentSpan().context().traceIdHigh(), tracer.currentSpan().context().traceId())
        .toString();
  }
}
