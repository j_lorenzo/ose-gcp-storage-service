package pe.net.tci.efacturacion.ose.gcpstorageservice.dto;

import lombok.Data;

@Data
public class ArchivoStorage {

  private String nombreArchivo;
  private String ublVersion;
  private byte[] archivo;
  private String path;
  private String tipo;
  private String archivoBase64;

  public ArchivoStorage(String nombreArchivo, String ublVersion, byte[] archivo, String path, String tipo) {
    this.nombreArchivo = nombreArchivo;
    this.ublVersion = ublVersion;
    this.archivo = archivo;
    this.path = path;
    this.tipo = tipo;
  }
}
