package pe.net.tci.efacturacion.ose.gcpstorageservice.repository;

import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import pe.net.tci.efacturacion.ose.commons.db.comprobante.tables.TxComprobanteArchivo;
import pe.net.tci.efacturacion.ose.commons.db.comprobante.tables.records.TxComprobanteArchivoRecord;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@Repository
public class TxComprobanteArchivoRepository {

  private final DSLContext dslContext;
  private final TxComprobanteArchivo TX_COMPROBANTE_ARCHIVO = TxComprobanteArchivo.TX_COMPROBANTE_ARCHIVO;

  public TxComprobanteArchivoRecord nuevoRecord() {
    return dslContext.newRecord(TxComprobanteArchivo.TX_COMPROBANTE_ARCHIVO);
  }

  public void registarArchivosXmlYCdr(TxComprobanteArchivoRecord txComprobanteArchivoRecord) {
    dslContext.executeInsert(txComprobanteArchivoRecord);
  }

  public TxComprobanteArchivoRecord obtenerPorNomArchivoXml(String nomArchivoXml) {
    return dslContext.fetchOne(TX_COMPROBANTE_ARCHIVO, TX_COMPROBANTE_ARCHIVO.NOM_ARCHIVO_XML.eq(nomArchivoXml));
  }

  public void actualizarEstadoArchivoAGuardadoEnStorage(String nomArchivoXml, String tipoArchivo) {
    dslContext
        .update(TX_COMPROBANTE_ARCHIVO)
        .set("ARCHIVO_XML".equals(tipoArchivo) ? TX_COMPROBANTE_ARCHIVO.WS_STORAGE_XML : TX_COMPROBANTE_ARCHIVO.WS_STORAGE_CDR,
            Byte.valueOf("1"))
        .where(TX_COMPROBANTE_ARCHIVO.NOM_ARCHIVO_XML.eq(nomArchivoXml))
        .execute();
  }

  public void eliminarRegistrosGuardadosEnStorageCorrectamentePorNroDias(Integer nroDias) {
    dslContext
        .deleteFrom(TX_COMPROBANTE_ARCHIVO)
        .where(TX_COMPROBANTE_ARCHIVO.WS_STORAGE_XML.eq(Byte.valueOf("1")), TX_COMPROBANTE_ARCHIVO.WS_STORAGE_CDR.eq(Byte.valueOf("1")))
        .and(DSL.cast(TX_COMPROBANTE_ARCHIVO.FEC_REGISTRO, LocalDate.class).lessOrEqual(LocalDate.now().minusDays(nroDias)))
        .execute();
  }

  public List<String> obtenerArchivosNoGuardadosEnStorage(){
    return dslContext
        .select(TX_COMPROBANTE_ARCHIVO.NOM_ARCHIVO_XML)
        .from(TX_COMPROBANTE_ARCHIVO)
        .where(TX_COMPROBANTE_ARCHIVO.WS_STORAGE_XML.eq(Byte.valueOf("0")))
        .or(TX_COMPROBANTE_ARCHIVO.WS_STORAGE_CDR.eq(Byte.valueOf("0")))
        .fetch(TX_COMPROBANTE_ARCHIVO.NOM_ARCHIVO_XML);
  }
}
